using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace dotnetcore.Infractructure.Serializators;

public abstract class StringConverter
{
    private const string Separator = ";";
    private const string FieldSeparator = "%FIELD%";
    private const string PropertySeparator = "%PROPERTY%";
    private const BindingFlags BindingAttribute = 
        BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public;

    /// <summary>
    /// Сериализовать объект.
    /// </summary>
    /// <param name="obj">Объект</param>
    /// <returns>Сериализованные данные</returns>
    public static string Serialize(object obj)
    {
        var data = new StringBuilder();
        var type = obj.GetType();

        data.Append($"typeName:{type.FullName}");
        
        // Сериализируем свойства.
        var properties = type.GetProperties(BindingAttribute);
        if (properties.Any())
        {
            data.Append(Separator);
            data.AppendJoin(Separator, properties.Select(p => $"{PropertySeparator}{p.Name}:{p.GetValue(obj) ?? string.Empty}"));
        }

        // Сериализируем поля.
        var fieldsInfo = type.GetFields(BindingAttribute);
        if (fieldsInfo.Any())
        {
            data.Append(Separator);
            data.AppendJoin(Separator, fieldsInfo
                .Select(f => $"{FieldSeparator}{f.Name}:{f.GetValue(obj) ?? string.Empty}"));
        }

        return data.ToString();
    }
    
    /// <summary>
    /// Десериализировать объект.
    /// </summary>
    /// <param name="str">Сериализованные данные</param>
    /// <returns>Десериализованный объект</returns>
    public static object Desserialize(string str)
    {
        var data = str.Split(Separator).ToDictionary(k => k.Split(":").First(), v => v.Split(":").LastOrDefault());

        string typeName;
        if (!data.TryGetValue("typeName", out typeName))
            throw new Exception("Type name is not found");

        var type = Type.GetType(typeName);
        if (type is null)
            throw new Exception($"Can't get type wich name {typeName}");

        var obj = Activator.CreateInstance(type);
        
        // Поля.
        foreach (var key in data.Keys.Where(k => k.StartsWith(FieldSeparator)))
        {
            var fieldInfo = type.GetField(key.Replace(FieldSeparator, string.Empty), BindingAttribute);
            if (fieldInfo is null)
                continue;
            
            fieldInfo.SetValue(obj, Convert.ChangeType(data[key], fieldInfo.FieldType));
        }
        
        // Свойства.
        foreach (var key in data.Keys.Where(k => k.StartsWith(PropertySeparator)))
        {
            var propertyInfo = type.GetProperty(key.Replace(PropertySeparator, string.Empty), BindingAttribute);
            if (propertyInfo is null)
                continue;
            
            propertyInfo.SetValue(obj, Convert.ChangeType(data[key], propertyInfo.PropertyType));
        }

        return obj;
    }

    /// <summary>
    /// Дессериализировать объект.
    /// </summary>
    public async static Task<T> Desserialize<T>(FileStream stream, CancellationToken token = default(CancellationToken)) 
        where T : class
    {
        if (stream.CanRead)
            throw new Exception($"File can't read.");
                
        var buffer = new byte [stream.Length];
        if (stream.Length != await stream.ReadAsync(buffer, 0, (int)stream.Length, token))
            throw new Exception($"An error occurred while reading.");

        return Desserialize(Encoding.ASCII.GetString(buffer)) as T;
    }
}