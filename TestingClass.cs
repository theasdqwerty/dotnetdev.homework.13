namespace dotnetcore;

public class TestingClass
{
    public int _i1;
    public int _i2;
    public int _i3;
    public int _i4;
    public int _i5;
    
    public static TestingClass Get()
        => new TestingClass() {_i1 = 1, _i2 = 2, _i3 = 3, _i4 = 4, _i5 = 5};

    public override string ToString() => 
        $"i1:{_i1};i2:{_i2};i3:{_i3};i4:{_i4};i5:{_i5}";
}