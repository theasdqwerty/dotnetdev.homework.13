﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using dotnetcore.Infractructure.Serializators;
using Newtonsoft.Json;

namespace dotnetcore
{
    public static class Program
    {
        /// <summary>
        /// Замер скорости сериализации в строку.
        /// </summary>
        private static void TimeSerializationToString()
        {
            Console.WriteLine("***************** Serialization to string *****************");
            var stopwatch = new Stopwatch();
            var elapsedTime = new List<TimeSpan>();
            
            var originalObject = TestingClass.Get();

            for (var i = 0; i < 100; i++)
            {
                stopwatch.Restart();
                StringConverter.Serialize(originalObject);
                stopwatch.Stop();
                
                elapsedTime.Add(stopwatch.Elapsed);
            }

            Console.WriteLine($"100 iterations elapsed time " +
                              $"min:{elapsedTime.Min()} max:{elapsedTime.Max()}");

            elapsedTime.Clear();
            
            for (var i = 0; i < 100000; i++)
            {
                stopwatch.Restart();
                StringConverter.Serialize(originalObject);
                stopwatch.Stop();
                
                elapsedTime.Add(stopwatch.Elapsed);
            }

            Console.WriteLine($"100000 iterations elapsed time " +
                              $"min:{elapsedTime.Min()} max:{elapsedTime.Max()}");
        }

        /// <summary>
        /// Замер скорости дессерилиализации из строки.
        /// </summary>
        private static void TimeDessesializationFromString(string data)
        {
            Console.WriteLine("***************** Desserialization from string *****************");

            var stopwatch = new Stopwatch();
            var elapsedTime = new List<TimeSpan>();

            for (int i = 0; i < 100; i++)
            {
                stopwatch.Restart();
                StringConverter.Desserialize(data);
                stopwatch.Stop();
                
                elapsedTime.Add(stopwatch.Elapsed);
            }
            
            Console.WriteLine($"100 iterations elapsed time " +
                              $"min:{elapsedTime.Min()} max:{elapsedTime.Max()}");
            
            elapsedTime.Clear();
            
            
            for (var i = 0; i < 100000; i++)
            {
                stopwatch.Restart();
                StringConverter.Desserialize(data);
                stopwatch.Stop();
                
                elapsedTime.Add(stopwatch.Elapsed);
            }

            Console.WriteLine($"100000 iterations elapsed time " +
                              $"min:{elapsedTime.Min()} max:{elapsedTime.Max()}");
        }
        
        /// <summary>
        /// Замер скорости сериализации в JSON.
        /// </summary>
        private static void TimeSerializationToJson()
        {
            Console.WriteLine("***************** Serialization to json *****************");

            var stopwatch = new Stopwatch();
            var elapsedTime = new List<TimeSpan>();
            
            var originalObject = TestingClass.Get();

            for (var i = 0; i < 100; i++)
            {
                stopwatch.Restart();
                JsonConvert.SerializeObject(originalObject);
                stopwatch.Stop();
                
                elapsedTime.Add(stopwatch.Elapsed);
            }
            
            Console.WriteLine($"100 iterations elapsed time " +
                              $"min:{elapsedTime.Min()} max:{elapsedTime.Max()}");
            
            elapsedTime.Clear();
            
            for (var i = 0; i < 100000; i++)
            {
                stopwatch.Restart();
                JsonConvert.SerializeObject(originalObject);
                stopwatch.Stop();
                
                elapsedTime.Add(stopwatch.Elapsed);
            }

            Console.WriteLine($"100000 iterations elapsed time " +
                              $"min:{elapsedTime.Min()} max:{elapsedTime.Max()}");
        }

        /// <summary>
        /// Замер скорости дессириализации из JSON.
        /// </summary>
        private static void TimeDesserializationFromJson(string data)
        {
            Console.WriteLine("***************** Desserializetion from json *****************");
            
            var stopwatch = new Stopwatch();
            var elapsedTime = new List<TimeSpan>();
                        
            for (var i = 0; i < 100; i++)
            {
                stopwatch.Restart();
                JsonConvert.DeserializeObject<TestingClass>(data);
                stopwatch.Stop();
                            
                elapsedTime.Add(stopwatch.Elapsed);
            }
                        
            Console.WriteLine($"100 iterations elapsed time " +
                              $"min:{elapsedTime.Min()} max:{elapsedTime.Max()}");
                        
            elapsedTime.Clear();
                        
            for (var i = 0; i < 100000; i++)
            {
                stopwatch.Restart();
                JsonConvert.DeserializeObject<TestingClass>(data);
                stopwatch.Stop();
                            
                elapsedTime.Add(stopwatch.Elapsed);
            }
            
            Console.WriteLine($"100000 iterations elapsed time " +
                              $"min:{elapsedTime.Min()} max:{elapsedTime.Max()}");
        }
        
        private static void Main()
        {
            var originalObject = TestingClass.Get();
            
            Console.WriteLine($"Original testing object: {originalObject}");
            
            // 1. Сериализация строк и полей в строку.
            // 2. Проверка на тестовом классе.
            Console.WriteLine($"Serializable testing class to string: {StringConverter.Serialize(originalObject)}");
            
            // 3. Замер скорости сериализации на 100-100000 итериациях.
            // 4. Вывод в консоль разници.
            TimeSerializationToString();
            
            // 5. 
            // 6. 
            // 7. Сериализация в json.
            TimeSerializationToJson();
            
            // 8. Замер скорости дессириализации.
            // Замер скорости дессериализации из строки.
            TimeDessesializationFromString(StringConverter.Serialize(TestingClass.Get()));
            // Замер скорости дессириализации из JSON.
            TimeDesserializationFromJson(JsonConvert.SerializeObject(TestingClass.Get(), Formatting.Indented));
            
            Console.ReadKey(true);
        }
    }
}